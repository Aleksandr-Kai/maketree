const nodeIdPrefix = "n";

function createTreeNode(data, collapsed = false) {
    const li = document.createElement("li");
    const name = document.createElement("span");
    name.textContent = `${data.name} (${data.price})`;
    li.appendChild(name);
    if (data.node) {
        const ul = document.createElement("ul");
        ul.id = nodeIdPrefix + data.id;
        if (collapsed) ul.classList.add("collapsed");
        name.addEventListener("click", () => {
            ul.classList.toggle("collapsed");
        });
        name.classList.add("subtree");
        li.appendChild(ul);
    } else {
        name.classList.add("treelist");
        li.id = nodeIdPrefix + data.id;
    }

    return li;
}

function sortUL(ul) {
    const sorted = [...ul.children].sort((a, b) => {
        return b.children.length - a.children.length;
    });
    ul.innerHTML = "";
    for (let item of sorted) ul.appendChild(item);
}

function buildTree(rootNode, data, collapsedAll = false, sort = false) {
    const root = document.createElement("ul");
    data.services.forEach((service) => {
        if (service.head) {
            const head = root.querySelector(`#${nodeIdPrefix + service.head}`);
            head.appendChild(createTreeNode(service, collapsedAll));
        } else {
            root.appendChild(createTreeNode(service, collapsedAll));
        }
    });
    if (sort) [root, ...root.querySelectorAll("ul")].forEach((ul) => sortUL(ul));
    rootNode.appendChild(root);
}

buildTree(document.body, api_data, false, true);
